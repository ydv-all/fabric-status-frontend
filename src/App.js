import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Header from './components/header/header';
import Footer from './components/footer/footer';
import StatusSection from './components/status-section/status-section';


function App() {
    return (
      <div className="App" >
        <Header />
        <StatusSection />
        <Footer />
      </div>
    );
}

export default App;