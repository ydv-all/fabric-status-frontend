import React from 'react'
import './footer.css'

function Footer() {
  return (
    <footer>
      © Fabric Copyright {new Date().getFullYear()} All rights reserved
    </footer>
  )
}

export default Footer