import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { Alert, Accordion, Card, Button, ListGroup } from 'react-bootstrap'
import { Check } from 'react-bootstrap-icons';


import Env from '../../env'

import './status-section.css'

function StatusSection() {
  const [endpoints, setEndpoints] = useState([]);

  useEffect(() => {
    getStatus()
  }, [])

  const getStatus = () => {
    axios
      .get(`${Env.API_URL}/status`)
      .then(response => {
        setEndpoints(response.data)
      })
      .catch(error => {
          console.log(error)
      })
  }

  return (
    <section>
      <Alert variant={'success'}>
        All Systems Operational
      </Alert>
      <section>
      <Accordion>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="0">
              Copilot application
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <ListGroup variant="flush">
                {endpoints.map((item, index) => {
                  return <ListGroup.Item key={`${item}-${index}`} className="endpoint">
                    <div className="endpoint-name">{item}</div>
                    <Check color="green" size={25}/>
                  </ListGroup.Item>
                })}
              </ListGroup>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
      <Accordion>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="0">
              Customer specific commerce tier
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <ListGroup variant="flush">
                <ListGroup.Item>To be added</ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
      <Accordion>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="0">
              Databases
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <ListGroup variant="flush">
                <ListGroup.Item>To be added</ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
      <Accordion>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="0">
              Integration
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <ListGroup variant="flush">
                <ListGroup.Item>To be added</ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
      <Accordion>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="0">
              Backend systems
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <ListGroup variant="flush">
                  <ListGroup.Item>To be added</ListGroup.Item>
                </ListGroup>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
      </section>
    </section>
  )
}

export default StatusSection