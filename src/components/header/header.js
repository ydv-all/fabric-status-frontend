import React from 'react'
import Logo from '../logo/logo'
import { Button } from 'react-bootstrap'


import './header.css'

function Header() {
  return (
    <header>
      <div className="logo-container">
        <Logo />
      </div>
      <h1 className="page-title">Fabric status</h1>
      <Button variant="primary">Contact support</Button>
    </header>)
}

export default Header;