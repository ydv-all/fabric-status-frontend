// TODO: figure out proper urls

export default {
    BASE_URL: process.env.NODE_ENV === "development" ? "http://localhost:3000" : "https://platform.status.zone",
    API_URL: process.env.NODE_ENV === "development" ? "https://rc3jxwm5t6.execute-api.us-east-1.amazonaws.com/dev" : 'https://rc3jxwm5t6.execute-api.us-east-1.amazonaws.com/dev'
}